PROG        := ready
PREFIX      ?= /usr/local
GO_MD2MAN   ?= go-md2man
CFLAGS      += -Iinclude -std=c99 -pedantic -Wall -Wextra -Werror

MANPAGES_MD := $(wildcard docs/man/*.md)
MANPAGES    := $(MANPAGES_MD:%.md=%)

BINDIR=$(DESTDIR)$(PREFIX)/bin
MANDIR=$(DESTDIR)$(PREFIX)/share/man/man8

SRC = src/main.c
OBJ = $(SRC:.c=.o)

default: $(PROG) $(MANPAGES)

$(PROG): $(OBJ)
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $^ $(LDLIBS)

$(MANPAGES): %: %.md
	$(GO_MD2MAN) -in $< -out $@

install: install.bin install.docs

install.bin: $(PROG)
	install -dZ -m 755 $(BINDIR)
	install -DZ -m 755 ./$(PROG) -t $(BINDIR)

install.docs: docs
	install -dZ -m 755 $(MANDIR)
	install -DZ -m 644 $(MANPAGES) -t $(MANDIR)

uninstall:
	rm -f $(BINDIR)/$(PROG)
	for i in $(MANPAGES); do \
		rm -f $(MANDIR)/$$(basename $${i}); \
	done

clean:
	rm -vfr *~ $(PROG) $(MANPAGES) $(OBJ)

.PHONY: default install install.bin install.docs uninstall docs clean
