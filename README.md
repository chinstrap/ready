# ready

`READY_FD` is a service readiness notification mechanism.

The protocol specification and the reference implementation of the protocol are
housed in this repository.

`docs/specification.md` contains a justification for the protocol, a description
of how it functions, and details on how to support it in a provider or consumer.

`docs/man/ready.8.md` contains information about how to use the reference
implementation.

If you would like to request a patch for a service or write a patch yourself,
refer to the #1 issue on this project page:
<https://gitlab.com/chinstrap/ready/issues/1>
