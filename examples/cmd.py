#!/usr/bin/python3

import os, sys

def get_fd_from_env(variable):
    '''
    Return the file descriptor described by the environment variable

    Args:
    variable -- Environment variable to check
    '''
    fdesc_str = os.getenv(variable)
    if fdesc_str is None or fdesc_str == "":
        raise ValueError("Environment variable unset or empty")
    fdesc = int(fdesc_str)
    os.unsetenv(variable)
    return fdesc

def main():
    ready_fd = get_fd_from_env("READY_FD")
    input("Press enter to write newline to READY_FD.")
    os.write(int(ready_fd), b'\n')
    input("Press enter to exit.")

if __name__ == '__main__':
    main()
