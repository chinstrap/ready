# Background

## Service Readiness

Historically, UNIX services were implemented as daemons.
These are long running processes that fork into the background.
When dependency based init systems became common, this daemonization process
was used as an indicator that the service was ready to accept requests from
dependent services.

While this is functional, the mechanism is not without flaws.
Most notably, forking into the background impedes the ability of a service
supervisor to monitor the service in case it exits uncleanly.
For this reason, services should have a way to indicate that they are ready
without forking into the background.

## Inter-Process Communication

Forking to indicate service readiness takes advantage of one of the most
primitive forms of IPC on UNIX systems: signals.
Signals have a number of disadvantages in comparison to file descriptor based
IPC mechanisms:

 - there is a limited number of signals that can be used, and most already
   have a predefined meaning,
 - delivery of signals other than SIGCHLD requires using process IDs in a
   potentially racy manner,
 - reacting to a signal is cumbersome and often a pain point for writers of
   UNIX software.

Any non-forking readiness mechanism should take these concerns into account,
and consider the merits of file descriptor based IPC instead.
Importantly, file descriptors can be passed by a parent supervisory process
as an unnamed token.
The parent can know definitively where an IPC message is coming from,
and the service has few responsibilities other than consuming the token.
File descriptors operate more transparently across namespace boundaries,
making them more useful for containerized services.

# Implementation

## Provider

A __provider__ is any process that wishes to wait for a readiness notification
from one of its child processes.
The provider creates an IPC object of its choice.
The read end of the object is retained by the provider and closed in the child.
The `READY_FD` environment variable is set to the value of the file descriptor
representing the write end of the IPC object.

Examples of providers include: service supervisors and container runtimes.

## Consumer

A __consumer__ is a process that has been spawned with the expectation that it
will notify the provider when certain 'readiness' conditions have been met.
The significance of readiness is defined by the consumer.

First, the consumer checks if the `READY_FD` variable is set, and if so attempts
to convert the variable to an integer.
Depending on how the consumer is configured, the variable not being set could
be considered an error, a warning, or simply the absence of a request for
readiness notifications.
Similarly, the variable not containing an integer could be considered a warning
or an error.

Next, the consumer retains the converted integer value from the `READY_FD`
environment variable and unset this variable.

Finally, the consumer writes a single newline character to the file descriptor
and closes the file descriptor when its self-defined readiness conditions have
been satisfied.

If an error occurs during these steps, the consumer should either:

 - promptly exit without executing any child processes,
 - unset the `READY_FD` environment variable and close the relevant file
   descriptor, if applicable.

Examples of possible consumers include:

 - services that verify the validity of their configuration,
 - services that expose interfaces that other processes consume,
 - tasks that reach a certain threshold of completion but defer termination.

