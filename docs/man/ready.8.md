% ready(8)
% Cameron Nemo
% DECEMBER 2018
# NAME
ready - start a service and wait for it to indicate readiness

# SYNOPSIS
**ready** [**-f**] [**-t** *path*] -- *command* [**command arguments**]

# DESCRIPTION

**ready** will start a service, in the foreground or background, and
wait for the service to indicate readiness by writing a newline into
the file descriptor whose number is stored in the *READY_FD*
environment variable.

# OPTIONS

With no options, **ready** will daemonize before starting the service
and wait in the foreground for the readiness notification.

When **-f** is specified, **ready** will instead execute the service
in the foreground, and wait for the notification in the background.
This behavior is useful in conjunction with the **-t** option, which
helps signal other processes that the service is ready by touching
the specified *path*.

# EXAMPLES

Systemd service:

```
[Unit]
Description=Example service using READY_FD

[Service]
Type=forking
ExecStart=ready -- example-service --options args
```

Upstart job:

```
description "Example job using READY_FD"
expect daemon
exec ready -- example-service --options args
```

runit or daemontools service:

```
$ cd /etc/sv/example-service
$ cat run
#!/bin/sh
exec ready -f -t ready -- example-service --options args
$ cat check
#!/bin/sh
exec test -e ready
$ cat finish
#!/bin/sh
exec rm -f -- ready
```

With s6, **ready** is not necessary. The *READY_FD* protocol can be
emulated using the notification file descriptor supported by s6.

```
$ cd /etc/sv/example-service
$ cat notification-fd
3
$ cat run
#!/bin/sh
export READY_FD=3
exec example-service --options args
```

# SEE ALSO

https://gitlab.com/chinstrap/ready/blob/master/docs/specification.md

https://skarnet.org/software/s6/notifywhenup.html

# COPYRIGHT

Copyright (C) 2018-2019 Cameron Nemo

This is free software, see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE
