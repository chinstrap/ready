#ifndef LOG_H
#define LOG_H

#include <stdio.h>

typedef enum {
	LOG_LVL_FTAL = 0,
	LOG_LVL_ERRO,
	LOG_LVL_WARN,
	LOG_LVL_INFO,
	LOG_LVL_DBUG
} log_lvl_t;

log_lvl_t log_lvl = 0;

#define LOG(lvl, fp, prefix, ...) \
	if (log_lvl >= lvl) \
		do { \
			fprintf(fp, prefix); \
			fprintf(fp, "%s():%d ", __func__, __LINE__); \
			fprintf(fp, __VA_ARGS__); \
			fprintf(fp, "\n"); \
		} while (0)

#define LOG_DBUG(...) LOG(LOG_LVL_DBUG, stderr, "DBUG: ", __VA_ARGS__)
#define LOG_INFO(...) LOG(LOG_LVL_INFO, stderr, "NOTE: ", __VA_ARGS__)
#define LOG_WARN(...) LOG(LOG_LVL_WARN, stderr, "WARN: ", __VA_ARGS__)
#define LOG_ERRO(...) LOG(LOG_LVL_ERRO, stderr, "ERRO: ", __VA_ARGS__)
#define LOG_FTAL(...) LOG(LOG_LVL_FTAL, stderr, "FTAL: ", __VA_ARGS__)

#endif /* LOG_H */
