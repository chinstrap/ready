#define _XOPEN_SOURCE 700
#include <errno.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>

#include "log.h"

#define READY_OPTSTR "+hVft:"
#define READY_VERSION "0.1.0"
#define READY_USAGE "[-f] [-t path] command [command arguments]"

/**
 * touch_file:
 * @touch: path of file to touch
 *
 * Opens the specified file for writing then immediately closes it.
 *
 * Returns: true on success, false on failure.
 **/
static bool touch_file(char *touch)
{
	FILE *stream;

	if (!touch) return true;

	stream = fopen(touch, "w");

	if (!stream) {
		LOG_ERRO("fopen: %s", strerror(errno));
		return false;
	}

	if (fclose(stream) == 0) return true;

	LOG_ERRO("fclose: %s", strerror(errno));
	return false;
}

/**
 * read_pipe:
 * @fd: file descriptor to read from
 *
 * Reads from the specified file descriptor until a newline is written
 * or the other end of the pipe is closed.
 *
 * Returns: true on newline received, false on EOF or other error.
 **/
static bool read_pipe(int fd)
{
	size_t size = 128;
	char buf[size];
	ssize_t ret;

	while ((ret = read(fd, buf, size)) != 0) {
		if (ret < 0) {
			if (errno == EINTR) continue;
			LOG_ERRO("read: %s", strerror(errno));
			return false;
		}

		if (strchr(buf, '\n')) return true;
	}

	LOG_ERRO("read: EOF");
	return false;
}

/**
 * set_env_var:
 * @fd: file descriptor to document
 *
 * Document the value of @fd in the READY_FD environment variable.
 *
 * Returns: true on success, false on failure.
 **/
static bool set_env_var(int fd)
{
	/* 20 for the width of 2^64, 1 for the NULL byte */
	char buf[21] = "";
	int ret;

	if (fd < 0) return false;

	ret = snprintf(buf, 21, "%d", fd);

	if (ret < 0 || ret >= 21) {
		LOG_ERRO("snprintf");
		return false;
	}

	if (setenv("READY_FD", buf, 1) == 0) return true;

	LOG_ERRO("setenv: %s", strerror(errno));
	return false;
}

static void run_command(char** argv)
{
	execvp(*argv, argv);
	LOG_ERRO("execvp: %s", strerror(errno));
	exit(EXIT_FAILURE);
}

static pid_t daemonize()
{
	pid_t ret;

	ret = fork();

	if (ret < 0) {
		LOG_ERRO("fork: %s", strerror(errno));
		return ret;
	}

	if (ret > 0) return ret;

	ret = setsid();

	if (ret < 0) {
		LOG_ERRO("setsid: %s", strerror(errno));
		return ret;
	}

	ret = fork();

	if (ret < 0) {
		LOG_ERRO("fork: %s", strerror(errno));
		return ret;
	}

	if (ret > 0) exit(EXIT_SUCCESS);

	return ret;
}

int main(int argc, char** argv)
{
	int opt;
	int fds[2];
	pid_t ret;
	char *touch;
	bool foreground;

	foreground = false;
	touch = NULL;

	while ((opt = getopt(argc, argv, READY_OPTSTR)) >= 0) {
		switch (opt) {
			case 'h':
				fprintf(stdout, "Usage: %s " READY_USAGE "\n",
						argv[0]);
				return EXIT_SUCCESS;
			case 'V':
				fprintf(stdout, READY_VERSION "\n");
				return EXIT_SUCCESS;
			case 'f':
				foreground = true;
				break;
			case 't':
				touch = optarg;
				break;
			default:
				fprintf(stderr, "Usage: %s " READY_USAGE "\n",
						argv[0]);
				return 2;
		}
	}

	if (*(argv + optind) == NULL) {
		fprintf(stderr, "Usage: %s " READY_USAGE "\n", argv[0]);
		return 2;
	}

	log_lvl = LOG_LVL_ERRO;

	if (pipe(fds) != 0) {
		LOG_ERRO("pipe: %s", strerror(errno));
		return EXIT_FAILURE;
	}

	ret = daemonize();

	if (ret < 0) return EXIT_FAILURE;

	if (foreground ^ (ret == 0)) {
		if (ret != 0 && waitpid(ret, NULL, 0) < 0) {
			LOG_ERRO("waitpid: %s", strerror(errno));
			return EXIT_FAILURE;
		}

		if (close(fds[0]) != 0) {
			LOG_ERRO("close: %s", strerror(errno));
			return EXIT_FAILURE;
		}

		if (!set_env_var(fds[1])) return EXIT_FAILURE;

		run_command(argv + optind);
	}

	if (close(fds[1]) != 0) {
		LOG_ERRO("close: %s", strerror(errno));
		return EXIT_FAILURE;
	}

	if (!read_pipe(fds[0])) return EXIT_FAILURE;
	if (!touch_file(touch)) return EXIT_FAILURE;
}
